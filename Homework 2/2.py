import time


class MyTimer(object):

    def __init__(self, units):
        self.units = units
        self.start_time = time.time()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.spent_time = time.time() - self.start_time

    def elapsed_time(self):
        if self.units == "s":
            return self.spent_time
        if self.units == "m":
            return self.spent_time / 60
        if self.units == "h":
            return self.spent_time / 3600


if __name__ == "__main__":
    with MyTimer(units="s") as t:
        time.sleep(5)

    print(t.elapsed_time())
