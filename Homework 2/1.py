class MedianFinder:
    def __init__(self):
        self.nums = []

    def find_median(self):
        if len(self.nums) % 2 == 1:
            return self.nums[len(self.nums) // 2]
        elif len(self.nums) == .0:
            return 0
        else:
            return (self.nums[len(self.nums) // 2 - 1] + self.nums[
                len(self.nums) // 2]) / 2

    def add_num(self, new_number):
        self.nums.append(new_number)
        self.nums.sort()


if __name__ == "__main__":
    median_finder = MedianFinder()
    assert median_finder.find_median() == .0

    median_finder.add_num(1)
    median_finder.add_num(2)
    assert median_finder.find_median() == 1.5

    median_finder.add_num(3)
    assert median_finder.find_median() == 2.

    print("All good")
