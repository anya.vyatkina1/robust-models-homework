def solve(str):
    stack = []
    flag = True
    for ch in str:
        if ch in '({[':
            stack.append(ch)
        elif ch == ')' and len(stack) > 0 and stack[len(stack) - 1] == '(':
            stack.pop()
        elif ch == ']' and len(stack) > 0 and stack[len(stack) - 1] == '[':
            stack.pop()
        elif ch == '}' and len(stack) > 0 and stack[len(stack) - 1] == '{':
            stack.pop()
        elif ch in '}])':
            flag = False
            break
    if flag and len(stack) == 0:
        return True
    else:
        return False


if __name__ == "__main__":
    str = input()
    print(solve(str))
