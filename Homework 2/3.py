if __name__ == "__main__":
    n = 4
    times = [[2,1,1],[2,3,1],[3,4,1]]
    x = 2
    INF = 10 ** 10

    d = [INF for i in range(0, n + 1)]
    d[x] = 0
    for i in range(n):
        for edge in times:
            if d[edge[1]] > d[edge[0]] + edge[2]:
                d[edge[1]] = d[edge[0]] + edge[2]
    d[0] = -1
    max_time = max(d)
    if max_time == INF:
        print(-1)
    else:
        print(max_time)
