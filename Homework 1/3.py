if __name__ == "__main__":
    arr = list(map(int, input().split(',')))
    k = int(input())
    nums = [0 for i in range(max(arr) + 1)]
    for i in arr:
        nums[i] += 1
    nums_with_indexes = []
    for i in range(len(nums)):
        nums_with_indexes.append((nums[i], i))
    nums_with_indexes.sort(reverse=True)
    ans = []
    for i in range(k):
        ans.append(nums_with_indexes[i][1])
    print(ans)
